from flask import Flask, jsonify, request, session
from flask_restful import Api, Resource
import datetime

app = Flask(__name__)
api = Api(app)
app.permanent_session_lifetime = datetime.timedelta(minutes=5)


app.secret_key = 'super secret key'


class HelloWorld(Resource):
    def get (self):
        print("show UA -> ", dir(session))

        return {"Hello": "world"}

    @app.route('/visits-counter/')
    def visits():
        if 'visits' in session:
            session['visits'] = session.get('visits') + 1  # reading and updating session data
        else:
            session['visits'] = 1 # setting session data
        return "Total visits: {} \n <br>{}".format(session.get('visits'),  str(int(session['visits'])))

    
    @app.route('/delete-visits/')
    def delete_visits():
        session.pop('visits', None) # delete visits
        return 'Visits deleted'

class UserData(Resource): 
    def get (self):
        return  UserData.read_json_users()
    
    def read_json_users():
        with open("user_data.json") as f:
            users = json.load(f)
        f.close()
        return jsonify(users)
   
class UsersPosts(Resource):
    """
    : returns json data
    """
    def get (self):
        return UsersPosts.read_json_posts()
    
    def read_json_posts():
        with open("user_posts.json") as f:
            posts = json.load(f)
        f.close()
        return jsonify(posts)

api.add_resource(HelloWorld, '/')
api.add_resource(UserData, '/users')
api.add_resource(UsersPosts, '/posts')

if __name__ == '__main__':
    app.run (debug=True, host='0.0.0.0')

# docker build -t users_posts_image:latest .
# docker run -d -p 5000:5000 users_posts_image